var app = require('express')();
var $ = jQuery = require('jquery');
var csv = require('./jquery.csv.min.js');
var http = require('http');
var Client = require('node-rest-client').Client;
var fs = require('fs');
var serveStatic = require('serve-static');
var client = new Client();
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Encoding");
  res.header("Access-Control-Allow-Methods", "GET");
  next();
});
app.use('/app', serveStatic('../front/dist'));

app.get('/', function (req, res) {
  //get request data and format it
  var lat = req.query.lat;
  var lng = req.query.lng;
  if (!lat || !lng)
    return res.status(400).send();
  var y = req.query.y || 2014; //year
  var m = req.query.m || 5; //month
  if (m.length === 1)
    m = "0" + m; //add leading zero
  
  //variables for parallel http requests
  var index = 0;
  var allData = {};
  
  
  //get city of user
  client.get('http://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng, function (rawAddress) {
    var address = JSON.parse(rawAddress);
    var geoResults = [];
    //for each result for given latlong
    for (var geoResult = 0; geoResult<address.results.length; geoResult++) {
      //for part of that result
      for (var component = 0; component<address.results[geoResult].address_components.length; component++) {
        //check type of address component
        var selected = address.results[geoResult].address_components[component];
        if (selected.types.indexOf('political') > -1
           || selected.types.indexOf('locality') > -1
           || selected.types.indexOf('postal_town') > -1) {
          //selected Component of Result is of useful type: political,locality,postal_town
          if (selected.short_name.length > 2)
            geoResults.push(selected.short_name);
        }
      }
    }
    //delete duplicates in array
    for (var i = 0; i<geoResults.length; i++) {
      if (geoResults.indexOf(geoResults[i]) !== i)
        geoResults.splice(i, 1);
    }
    
    //-----
    //other api calls after retrieving location (geoResults array)
    //-----
  
    //weather at requested location
    var weatherUrl = 'http://api.openweathermap.org/data/2.5/weather?lat=' + lat + '&lon='+lng;
    client.get(weatherUrl, function (data, raw) {
      console.log(data);
      try {
      data = JSON.parse(data);
      } catch (e) {
        data = {weather: [{description: 'heavy rain'}], main: {humidity: 75}};  
      }
      allData.weather = {
        name: 'Weather',
        value: 3,
        adjective: data.weather[0].description
      };
//      var humVal = Math.floor(data.main.humidity / 25) + 1;
//      humVal = (humVal > 5) ? 5 : humVal;
//      var humAdj = '';
//      switch (humVal) {
//        case 1:
//          humAdj = 'super low';
//          break;
//        case 2:
//          humAdj = 'low';
//          break;
//        case 3:
//          humAdj = 'medium';
//          break;
//        case 4:
//          humAdj = 'high';
//          break;
//        case 5:
//          humAdj = 'tropical';
//          break;
//      }
//      allData.humidity = {
//        name: 'Humidity',
//        value: humVal,
//        adjective: humAdj
//      };
      index++;
      if (index == 4)
        processData(allData, res);
    });

    //crime data in requested area
    var crimeUrl = 'http://data.police.uk/api/crimes-street/all-crime?lat='+lat+'&lng='+lng+'&date='+y+'-'+m;
    client.get(crimeUrl, function (data, raw) {
      //anti-social
      var asb = 0;
      //check if it's a valid response
        data.forEach(function (el) {
          if (el.category == 'anti-social-behaviour')
            asb++;
        });
        var vic = 0;
        data.forEach(function (el) {
          if (el.category == 'violent-crime')
            vic++;
        });
        //turn anti social behaviour into expected values
        if (asb) {
          if (asb < 40)
            asb = 5;
          else if (asb < 120)
            asb = 4;
          else if (asb < 250)
            asb = 3;
          else if (asb < 400)
            asb = 2;
          else
            asb = 1;
        }
        var asbAdj = '';
        switch (asb) {
          case 5:
            asbAdj = 'almost no';
            break;
          case 4:
            asbAdj = 'few';
            break;
          case 3:
            asbAdj = 'a couple of';
            break;
          case 2:
            asbAdj = 'a lot of';
            break;
          case 1:
            asbAdj = 'tons of';
            break;
          case 0:
            asbAdj = 'absolutely no';
        }
        allData.antiSocialBehaviour = {
          name: 'Unfriendly People',
          value: asb,
          adjective: asbAdj
        };
        //turn violent crime data into expected values
        if (vic) {
          if (vic < 25)
            vic = 5;
          else if (vic < 90)
            vic = 4;
          else if (vic < 140)
            vic = 3;
          else if (vic < 220)
            vic = 2;
          else
            vic = 1;
        }
        var vicAdj = '';
        switch (vic) {
          case 5:
            vicAdj = 'very few';
            break;
          case 4:
            vicAdj = 'some';
            break;
          case 3:
            vicAdj = 'definitely enough';
            break;
          case 2:
            vicAdj = 'a lot of';
            break;
          case 1:
            vicAdj = 'just too much';
            break;
          case 0:
            vicAdj = 'absolutely no';
        }
        allData.violentCrime = {
          name: 'Violent Crime',
          value: vic,
          adjective: vicAdj
        };
      index++;
      if (index == 4)
        processData(allData, res);
    });
    
    //sherlock holmes data
    var sherlockUrl = 'http://api.trakt.tv/show/stats.json/55bd2c42490a4fdef326f8c09f53e582/sherlock-holmes';
    client.get(sherlockUrl, function (data, raw) {
      var sherlock = data.watchers;
      if (sherlock > 100)
        sherlock = 5;
      else if (sherlock > 75)
        sherlock = 4;
      else if (sherlock > 50)
        sherlock = 3;
      else if (sherlock > 25)
        sherlock = 2;
      else
        sherlock = 1;
      var sherlockAdj = '';
      switch (sherlock) {
        case 1:
          sherlockAdj = 'almost no';
          break;
        case 2:
          sherlockAdj = 'few';
          break;
        case 3:
          sherlockAdj = 'a couple of';
          break;
        case 4:
          sherlockAdj = 'quite a lot of';
          break;
        case 5:
          sherlockAdj = 'many';
          break;
      }
        allData.sherlockHolmes = {
          name: 'Sherlock Holmes Viewers',
          value: sherlock,
          adjective: sherlockAdj
        };
      index++;
      if (index == 4)
        processData(allData, res);
    });
    
    

    //broadband speed data
    fs.readFile('./broadband.csv', 'UTF-8', function(err, csv) {
      $.csv.toArrays(csv, {}, function (err, data) {
        var broadbandSpeed = 0;
        data.splice(0,1); //delete array with column names
        outerFor:
        for (var i = 0; i<data.length; i++) {
          for (var j = 0; j<geoResults.length; j++) {
            if (data[i][0].indexOf(geoResults[j]) > -1) {
              //matching city/town
              broadbandSpeed = data[i][1];
              break outerFor;
            }
          }
        }
        //map raw data to standardized value
        if (broadbandSpeed) {
          if (broadbandSpeed < 8)
            broadbandSpeed = 1;
          else if (broadbandSpeed < 12)
            broadbandSpeed = 2;
          else if (broadbandSpeed < 18)
            broadbandSpeed = 3;
          else if (broadbandSpeed < 23)
            broadbandSpeed = 4;
          else
            broadbandSpeed = 5;
        }
        //map value to english words
        var broadbandSpeedEN = '';
        switch (broadbandSpeed) {
          case 1:
            broadbandSpeedEN = 'poor';
            break;
          case 2:
            broadbandSpeedEN = 'bad';
            break;
          case 3:
            broadbandSpeedEN = 'standard';
            break;
          case 4:
            broadbandSpeedEN = 'good';
            break;
          case 5:
            broadbandSpeedEN = 'highspeed';
            break;
        }
        if (broadbandSpeed)
          allData.broadband = {
            name: 'Broadband Internet',
            value: broadbandSpeed,
            adjective: broadbandSpeedEN};
      });
      index++;
      if (index == 4)
        processData(allData, res);
    });
    
    
    
    
  });
});
var server = app.listen(8080);

function processData (data,res) {
  var result = [];
  if (data.broadband) {
    result.push({source: data.sherlockHolmes.name, target: data.broadband.name,
                 text: data.sherlockHolmes.adjective +' '+ data.sherlockHolmes.name + ' results in ' +
        data.broadband.adjective + ' ' + data.broadband.name});
    result.push({source: data.broadband.name, target: data.violentCrime.name,
                 text: data.broadband.adjective +' '+ data.broadband.name + ' leads to ' +
        data.violentCrime.adjective + ' ' + data.violentCrime.name});
  } else {
    result.push({source: data.sherlockHolmes.name, target: data.violentCrime.name,
                 text: data.sherlockHolmes.adjective +' '+ data.sherlockHolmes.name + ' results in ' +
        data.violentCrime.adjective + ' ' + data.violentCrime.name});
  }
  result.push({source: data.weather.name, target: data.antiSocialBehaviour.name,
               text: data.weather.adjective + ' leads to ' +
      data.antiSocialBehaviour.adjective + ' ' + data.antiSocialBehaviour.name});
  
  res.send(result);
}

/* 
1. zwei arrays da draus machen
2. eines src eines ziel
3. beide gleich lang
4. zusammenfügen

*/