'use strict';

angular
  .module('frontApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'mm.foundation',
    'mm.foundation.topbar',
    'ui.router',
    'google-maps'
  ])
  .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $urlRouterProvider
      .otherwise('/main');
    $stateProvider
      .state('main', {
        url: '/main',
        templateUrl: 'views/main.html'
      });
  });



//angular-Reparaturen
angular.module('frontApp')
  .factory('$restsource', ['$resource', function($resource){
    return function(url, paramDefaults, actions){
       var MY_ACTIONS = {
         'update':   {method:'PUT'}
       };
       actions = angular.extend({}, MY_ACTIONS , actions);
       return $resource(url, paramDefaults, actions);
    }
  }]);