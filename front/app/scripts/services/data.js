angular.module('frontApp')
  .factory('dataService', function ($http) {
    return {
      get: function (lat, lng, year, month, callback) {
        $http({method: 'GET', url: '/?lat='+lat+'&lng='+lng+'&year='+year+'&month='+month}).then(callback);
      }
    }
  });