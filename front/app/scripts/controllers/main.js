'use strict';

angular.module('frontApp')
  .controller('MainCtrl', function ($scope, dataService) {
    $scope.year = 2014;
    $scope.month = '05';
    $scope.waiting = false;
    $scope.map = {
      center: {
        latitude: 50.375703,
        longitude: -4.140733
      },
      zoom: 16,
      events: {
        click: function (map, event, eventArgs) {
          if ($scope.waiting)
            return;
          $scope.mapClicked = true;
          $scope.clickedLocation = {latitude: eventArgs[0].latLng.k, longitude: eventArgs[0].latLng.B};
          update();
        }
      }
    };
    $scope.changedDate = function () {
      if ($scope.mapClicked) {
        update();
      }
    };
    $scope.cards = [];
    var update = function () {
      if ($scope.waiting)
        return;
      var lat = $scope.clickedLocation.latitude, lng = $scope.clickedLocation.longitude;
      $scope.waiting = true;
      $scope.cards = [];
      dataService.get(lat, lng, $scope.year, $scope.month, function (data) {
        $scope.waiting = false;
        $scope.cards = data.data;
        console.log($scope.cards);
        $scope.clickedLocation= {latitude: lat, longitude: lng};
      });
    }
  });