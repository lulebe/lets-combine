angular.module('frontApp')
  .factory('crimeService', function ($http) {
    function getCrime (date, lat, lng, callback) {
      $http({method: 'GET',
        url: 'http://data.police.uk/api/crimes-street/all-crime?lat=' + lat + '&lng=' + lng + '&date=' + date})
        .success(callback);
    };
    var crime = {
      getAll: function (lat, lng, year, month, callback) {
        if (month.length < 2) month = '0' + month;
        getCrime(year + '-' + month, lat, lng, function (data) {
          var crimes = [];
          var asb = crime.antiSocialBehaviour(data);
          if (asb > 0)
            crimes.push({cat: 'anti-social behaviour', val: asb});
          var bt = crime.bicycleTheft(data);
          if (bt > 0)
            crimes.push({cat: 'bicycle theft', val: bt});
          var b = crime.burglary(data);
          if (b > 0)
            crimes.push({cat: 'burglary', val: b});
          var d = crime.drugs(data);
          if (d > 0)
            crimes.push({cat: 'drugs', val: d});
          var r = crime.robbery(data);
          if (r > 0)
            crimes.push({cat: 'robbery', val: r});
          var sl = crime.shoplifting(data);
          if (sl > 0)
            crimes.push({cat: 'shoplifting', val: sl});
          var tftp = crime.theftFromThePerson(data);
          if (tftp > 0)
            crimes.push({cat: 'theft from the person', val: tftp});
          var vec = crime.vehicleCrime(data);
          if (vec > 0)
            crimes.push({cat: 'vehicle crime', val: vec});
          var vic = crime.violentCrime(data);
          if (vic > 0)
            crimes.push({cat: 'violent crime', val: vic});
          callback(crimes);
        });
      },
      get: getCrime,
      antiSocialBehaviour: function (data) {
        var count = 0;
        data.forEach(function (el) {
          if (el.category == 'anti-social-behaviour')
            count++;
        });
        return count;
      },
      bicycleTheft: function (data) {
        var count = 0;
        data.forEach(function (el) {
          if (el.category == 'bicycle-theft')
            count++;
        });
        return count;
      },
      burglary: function (data) {
        var count = 0;
        data.forEach(function (el) {
          if (el.category == 'burglary')
            count++;
        });
        return count;
      },
      drugs: function (data) {
        var count = 0;
        data.forEach(function (el) {
          if (el.category == 'drugs')
            count++;
        });
        return count;
      },
      robbery: function (data) {
        var count = 0;
        data.forEach(function (el) {
          if (el.category == 'robbery')
            count++;
        });
        return count;
      },
      shoplifting: function (data) {
        var count = 0;
        data.forEach(function (el) {
          if (el.category == 'shoplifting')
            count++;
        });
        return count;
      },
      theftFromThePerson: function (data) {
        var count = 0;
        data.forEach(function (el) {
          if (el.category == 'theft-from-the-person')
            count++;
        });
        return count;
      },
      vehicleCrime: function (data) {
        var count = 0;
        data.forEach(function (el) {
          if (el.category == 'vehicle-crime')
            count++;
        });
        return count;
      },
      violentCrime: function (data) {
        var count = 0;
        data.forEach(function (el) {
          if (el.category == 'violent-crime')
            count++;
        });
        return count;
      }
    }
    return crime;
  });